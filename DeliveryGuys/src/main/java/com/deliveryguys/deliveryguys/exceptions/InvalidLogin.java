package com.deliveryguys.deliveryguys.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class InvalidLogin extends RuntimeException {
    public InvalidLogin(String message) {
        super(message);
    }
}