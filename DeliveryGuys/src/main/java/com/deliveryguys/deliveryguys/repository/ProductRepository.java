package com.deliveryguys.deliveryguys.repository;

import com.deliveryguys.deliveryguys.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query(value = "SELECT * FROM store WHERE id = :id", nativeQuery = true)
    List<String> getProductsById(@Param("id") Long id);

    @Query(value ="Select * FROM product ", nativeQuery = true)
    List<Product> getAllProducts();
}
