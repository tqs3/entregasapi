package com.deliveryguys.deliveryguys.repository;

import com.deliveryguys.deliveryguys.pojo.Register;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;


import com.deliveryguys.deliveryguys.model.User;
@Transactional
@Repository
public interface UserRepository extends JpaRepository<User,Long>  {
    
    @Query(value="Select * from user u where u.email = ?1", nativeQuery = true)
    User findByEmail(String email);

    @Query(value ="Select * from user ",nativeQuery = true)
    List <User> getUsers();

    @Query(value = "select email from user",nativeQuery = true)
    List <String> getEmails();

    @Query(value ="Select password from user where user.email = ?1", nativeQuery = true)
    String getPassword(String email);

    @Query(value = "Select userID from user where email =:email", nativeQuery = true)
    Long findIdByEmail(@Param("email") String email);

    @Query(value ="Select email from user where name =:name", nativeQuery = true)
    String getEmail(@Param("name") String name);

    @Query(value = "Select name from User where email =:email", nativeQuery = true)
    String getName(@Param("email") String email);

    @Query(value = "Select * from user where userID=:id", nativeQuery = true)
    User findByID(@Param("id") Long id);

    Register findUserByEmail(@Param("email") Register email);

}
