package com.deliveryguys.deliveryguys.repository;

import java.util.List;

import com.deliveryguys.deliveryguys.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query(value ="Select * FROM encomenda", nativeQuery = true)
    List<Order> getAllOrders();

    @Query(value = "Select * from encomenda where id=:id", nativeQuery = true)
    Order findByID(@Param("id") Long id);

    @Query(value = "Select * from encomenda where userID=:id", nativeQuery = true)
    List<Order> getUserDelivers(@Param("id") Long id);

}
