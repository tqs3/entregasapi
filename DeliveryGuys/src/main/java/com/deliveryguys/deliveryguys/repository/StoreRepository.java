package com.deliveryguys.deliveryguys.repository;

import com.deliveryguys.deliveryguys.model.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface StoreRepository extends JpaRepository<Store, Long> {

    @Query(value = "SELECT * FROM store WHERE id = :id", nativeQuery = true)
    /*List<String>*/ Store getStoresById(@Param("id") Long id);

    @Query(value ="Select * FROM store ", nativeQuery = true)
    List<Store> getAllStores();
}