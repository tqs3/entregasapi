package com.deliveryguys.deliveryguys.dto;

import com.deliveryguys.deliveryguys.model.Store;

public class StoreDTO {

    //Store's name
    private String name;
    private String type;

    public StoreDTO() {

    }

    //Construtor não incluia o type
    public StoreDTO(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public StoreDTO(String name) {
        this.name = name;
    }

    //Construtor feito ao corrigir o erro do StoreService (era vazio inicialmente)
    public StoreDTO(Store store) {
        this.name = store.getName();
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "StoreDTO{" +
                "store='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}