package com.deliveryguys.deliveryguys.dto;

import com.deliveryguys.deliveryguys.model.Product;

public class ProductDTO {

    private long productId;

    public ProductDTO(){
    }

    public ProductDTO(Product product) {
        //Construtor feito ao corrigir o erro do ProductService (era vazio inicialmente)
    }

    public long getProductId() {
        return productId;
    }

}
