package com.deliveryguys.deliveryguys.dto;

import java.util.Date;


import com.deliveryguys.deliveryguys.model.Order;
import com.deliveryguys.deliveryguys.model.User;

public class OrderDTO {
    private Long id;
    private Long userID;
    private String email;
    private Date date;
    private boolean state;
    private String status;


    public OrderDTO(Date date,boolean state, String status){
        this.date = date;
        this.state = state;
        this.status = status;
    }
    public OrderDTO(Order order, User user){
        this.id = order.getId();
        this.date = order.getDate();
        this.email = user.getEmail();
        this.state = order.isState();
        this.status = order.getStatus();
        this.userID = user.getUserID();
    }

    public OrderDTO(Long id,String email,Date date,boolean state, Long userId, String status) {
        this.id = id;
        this.date = date;
        this.email = email;
        this.state = state;
        this.userID = userId;
        this.status = status;

    }


    public OrderDTO(){

    }

    public OrderDTO(String toString, boolean b) {
    }

    public OrderDTO(long id, String email, String toString, boolean b) {
    }

    public Long getUserID() {
        return userID;
    }

    public Date getDate(){
        return date;
    }
    public boolean isState(){
        return state;
    }

    public String getEmail() {
        return email;
    }

    public long getOrderID() {
        return id;
    }

    public void setOrderID(long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Encomenda{" +
                "id=" + id +
                ", date=" + date +
                ", state=" + state +
                ", userID=" + userID +
                ", status=" + status +
                ", email= "+ email +
                '}';
    }
}