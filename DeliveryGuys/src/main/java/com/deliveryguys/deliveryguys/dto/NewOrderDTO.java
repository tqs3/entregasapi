package com.deliveryguys.deliveryguys.dto;

import java.util.Date;


public class NewOrderDTO {
    private Long id;
    private Long userID;
    private Date date;
    private boolean state;
    private String status;
    private String address;
    private Long storeID;


    public NewOrderDTO(Long id, Long userID, Date date, boolean state, String status, Long store,  String address){
        this.id = id;
        this.userID = userID;
        this.date = date;
        this.state = state;
        this.status = status;
        this.storeID = store;
        this.address = address;
    }

    public NewOrderDTO(Long id, Long userID, String toString, boolean state, String status){
    }

    public NewOrderDTO(){
        //default contructor
    }

    public Long getId() {
        return id;
    }

    public Long getUserID() {
        return userID;
    }

    public Date getDate() {
        return date;
    }

    public boolean isState() {
        return state;
    }

    public String getStatus() {
        return status;
    }

    public String getAddress() {
        return address;
    }

    public Long getStoreID() {
        return storeID;
    }
    

    @Override
    public String toString() {
        return "NewOrderDTO{" +
                ", id=" + id +
                ", userID=" + userID +
                ", date=" + date +
                ", state=" + state +
                ", status= WAITING" +
                ", storeID= "+ storeID +
                ", address =" + address +
                '}';
    }
}
