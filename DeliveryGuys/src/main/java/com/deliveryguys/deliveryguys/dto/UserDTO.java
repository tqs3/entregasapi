package com.deliveryguys.deliveryguys.dto;

import com.deliveryguys.deliveryguys.model.User;

public class UserDTO {
    private String email;
    private String name;
    private String password;

    public UserDTO(){

    }

    public UserDTO(User user){
        this.name = user.getName();
        this.password = user.getPassword();
        this.email = user.getEmail();

    }

    public UserDTO(String name ,String password, String email){
        this.name = name;
        this.password = password;
        this.email = email;

}

    public String getName(){
        return name;
    }

    public String getPassword(){
        return password;
    }

    public String getEmail(){
        return email;
    }

}