package com.deliveryguys.deliveryguys.model;

import com.deliveryguys.deliveryguys.dto.NewOrderDTO;
import com.deliveryguys.deliveryguys.dto.OrderDTO;
import com.fasterxml.jackson.annotation.JsonFormat;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Set;

import javax.persistence.JoinColumn;

import javax.persistence.*;

@Entity
@Transactional
@Table(name="encomenda")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat
    @Column(nullable=false)
    private Date date;

    @Column(nullable = false)
    private boolean state;

    @Column
    private String status;


    public Order() {

    }

    public Order(Date date, boolean state, String status) {
        this.date = date;
        this.state = state;
        this.status = status;

    }

    public Order(OrderDTO order, User user1) {
        this.date = order.getDate();
        this.state = order.isState();
        this.user = user1;
    }

    public Order(NewOrderDTO orderDTO, User user) {
        this.user = user;
        this.id = orderDTO.getId();
        this.date = orderDTO.getDate();
        this.state = orderDTO.isState();
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="userID",nullable = false)
    private User user;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name ="order_product", joinColumns={@JoinColumn(name="order_id")},
    inverseJoinColumns={@JoinColumn(name = "product_id")})
    private Set<Product> product;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="store_id")
    private Store store;


    public Order(String toString, boolean b){}

    public Long getId() {
        return id;
    }
    public User getUserID() {
        return user;
    }

    public Store getStore(){
        return store;
    }
    
    public Date getDate() {
        return date;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
    public void setId(Long id){
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", date=" + date +
                ", state=" + state +
                ", user=" + user +
                ", store=" + store +
                ", status=" + status +
                '}';
    }
}
