package com.deliveryguys.deliveryguys.model;

import java.util.Set;

import javax.persistence.*;

import com.deliveryguys.deliveryguys.dto.UserDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Entity(name="user")
@Table(name="user")
public class User {

    @Column(name="name",nullable=false, unique=false)
    private String name;

    @Column(name="password" ,nullable = false, unique = false)
    private String password;

    @Column(name ="email",nullable = false, unique = true)
    private String email;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long userID;

    public User () {
        
    }

    public User(String name ,String password,String email){
        this.name = name;
        this.password = password;
        this.email = email;

    }

    public User(UserDTO userDTO) {

        this.name = userDTO.getName();
        this.password = userDTO.getPassword();
        this.email = userDTO.getEmail();
        
    }

    @OneToMany(fetch = FetchType.LAZY ,mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<Order> orders;
    
    public void setID(long userID){
        this.userID = userID;
    }

    public Long getUserID(){
        return userID;
    }

    public String getName(){
        return name;
    }
  

    public String getEmail(){
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", userID=" + userID +
                ", orders=" + orders +
                '}';
    }
}