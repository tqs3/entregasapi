package com.deliveryguys.deliveryguys.model;

import com.deliveryguys.deliveryguys.dto.StoreDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.transaction.annotation.Transactional;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Transactional
@Entity
@Table(name="store")
public class Store {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="name", nullable=false, unique=false)
    private String name;

    @Column(nullable=false)
    private String type;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "store_product",
            joinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "store_id", referencedColumnName = "id"))
    private Set<Product> product = new HashSet<>();

    @OneToMany(mappedBy = "store")
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<Order> orders = new HashSet<>();

    public Store() {

    }

    public Store(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public Store(StoreDTO storeDTO) {
        this.name = storeDTO.getName();
        this.type = storeDTO.getType();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<Order> getOrder() {
        return orders;
    }

    public void addOrder(Order order){
        orders.add(order);
    }

    public void removeOrder(Order order){
        orders.add(null);
        this.orders.remove(order);
    }

    @Override
    public String toString() {
        return "Store{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                '}';
    }
}
