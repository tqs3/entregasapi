package com.deliveryguys.deliveryguys.service;

import com.deliveryguys.deliveryguys.dto.StoreDTO;
import com.deliveryguys.deliveryguys.exceptions.InvalidStore;
import com.deliveryguys.deliveryguys.exceptions.InvalidStoreId;
import com.deliveryguys.deliveryguys.model.Store;
import com.deliveryguys.deliveryguys.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.List;

@Service
public class StoreService {

    @Autowired
    private StoreRepository storeRepository;

    public List<Store> getAllStores() {
        return storeRepository.getAllStores();
    }

    public List<StoreDTO> storeToDTO(List<Store> stores){
        List<StoreDTO> toReturn = new ArrayList<>();
        for(Store store : stores){
            toReturn.add(new StoreDTO(store));
        }
        return toReturn;
    }

    public Store getStoresById(Long id) {
        try{
            return storeRepository.getStoresById(id);
        }catch (HttpClientErrorException.NotFound e){
            throw new InvalidStoreId();
        }
    }

    public Store addStore(StoreDTO storeDTO){
        try {
            Store store = new Store(storeDTO);
            storeRepository.save(store);
            return store;
        } catch (HttpClientErrorException.BadRequest e) {
            throw new InvalidStore();
        }
    }

    public Store save(Store store) {
        return storeRepository.save(store);
    }
}
