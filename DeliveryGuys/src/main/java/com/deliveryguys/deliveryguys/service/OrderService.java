package com.deliveryguys.deliveryguys.service;

import java.util.ArrayList;
import java.util.List;

import com.deliveryguys.deliveryguys.dto.NewOrderDTO;
import com.deliveryguys.deliveryguys.dto.OrderDTO;
import com.deliveryguys.deliveryguys.exceptions.InvalidInput;
import com.deliveryguys.deliveryguys.model.Order;
import com.deliveryguys.deliveryguys.model.User;
import com.deliveryguys.deliveryguys.repository.OrderRepository;
import com.deliveryguys.deliveryguys.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

@Transactional
@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserRepository userRepository;

    public List<OrderDTO> getAllOrders() {
        List<Order> allOrders = orderRepository.getAllOrders();
        return orderToDTO(allOrders);
    }

    public OrderDTO getOrderByID(Long id) {
        try {
            Order order = orderRepository.findByID(id);
            User user =order.getUserID();
            return new OrderDTO(order, user);
        } catch (HttpServerErrorException.InternalServerError e) {
            throw new InvalidInput();
        }

    }

    public Order newOrder(NewOrderDTO orderDTO) {
        try {
            Order order;
            User user = userRepository.findByID(orderDTO.getUserID());
            order = orderRepository.save(new Order(orderDTO, user));
            order.setStatus("ACCEPTED");
            orderRepository.save(order);
            return order;

        } catch (HttpClientErrorException.BadRequest | NullPointerException e){
            throw new InvalidInput();
        }
    }

    public List<OrderDTO> orderToDTO(List<Order> orders){
        List<OrderDTO> toReturn = new ArrayList<>();
        for(Order order : orders){
            User user = order.getUserID();
            toReturn.add(new OrderDTO(order, user));
        }
        return toReturn;
    }

    public boolean setDelivered(long id) {
        try {
            Order order = orderRepository.findByID(id);
            order.setState(true);
            order.setStatus("DELIVERED");
            orderRepository.save(order);

           return true;
        } catch (NullPointerException e) {
            return false;
        }
    }

    public  List<OrderDTO> getUserDelivers(String email) {
        Long userID = userRepository.findIdByEmail(email);
        List<Order> userDelivers = orderRepository.getUserDelivers(userID);
        return orderToDTO(userDelivers);

    }


    public Order verifyOrderStatus(Long orderId) {
        Order order1 = orderRepository.findByID(orderId);

        if(order1.getStatus().equals("ACCEPTED")) {
            order1.setStatus("IN TRANSIT");

        } else if(order1.isState()) {
            order1.setStatus("DELIVERED");

        }
        orderRepository.save(order1);

        return order1;
    }
}
