package com.deliveryguys.deliveryguys.service;

import com.deliveryguys.deliveryguys.dto.ProductDTO;
import com.deliveryguys.deliveryguys.exceptions.InvalidProductId;
import com.deliveryguys.deliveryguys.model.Product;
import com.deliveryguys.deliveryguys.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<ProductDTO> getAllProducts() {
        return productToDTO(productRepository.getAllProducts());
    }

    public List<ProductDTO> productToDTO(List<Product> products){
        List<ProductDTO> toReturn = new ArrayList<>();
        for(Product product : products){
            toReturn.add(new ProductDTO(product));
        }
        return toReturn;
    }

    public List<String> getProductsById(Long id) {
        try{
            return productRepository.getProductsById(id);
        }catch (HttpClientErrorException.NotFound e){
            throw new InvalidProductId();
        }
    }
}
