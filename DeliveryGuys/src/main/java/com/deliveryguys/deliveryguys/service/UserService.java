package com.deliveryguys.deliveryguys.service;

import java.util.List;

import com.deliveryguys.deliveryguys.dto.UserDTO;
import com.deliveryguys.deliveryguys.exceptions.InvalidInput;
import com.deliveryguys.deliveryguys.exceptions.InvalidUser;
import com.deliveryguys.deliveryguys.model.User;
import com.deliveryguys.deliveryguys.pojo.Login;
import com.deliveryguys.deliveryguys.pojo.Register;
import com.deliveryguys.deliveryguys.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.HttpClientErrorException;
import java.util.logging.*;

@Transactional
@Service
public class UserService {

    @Autowired  
    private UserRepository userRepository;

    Logger logger = Logger.getLogger(UserService.class.getName());

    public List<User> getUsers(){
        return userRepository.getUsers();
        
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User getUserByID(long id){
        return userRepository.findByID(id);
    }
    
    public List<String> getEmails() {
        return userRepository.getEmails();
    }

    public String getEmail(String name) {
        String email = userRepository.getEmail(name);
        if (email == null){
            logger.log(Level.WARNING, "Erro!!! Try Again");
            throw new InvalidUser();
        }
        return email;
    }

    public String getPassword(String email) {
        String pass = userRepository.getPassword(email);
        if (pass == null){
            logger.log(Level.WARNING, "Erro!!! Try Again");
            throw new InvalidUser();
        }
        return pass;

    }

    public boolean exists(String email) {
        return userRepository.findByEmail(email) != null;
    }

    public User addUser(UserDTO userDTO){
        try {
            User user = new User(userDTO);
            userRepository.save(user);
            return user;
        } catch (HttpClientErrorException.BadRequest e) {
            throw new InvalidUser();
            
        }
    }
    public User save(User user) {
        return userRepository.save(user);
    }

    public User saveUserPojo(Register userPojoRegister){
        User user = new User();

        user.setName(userPojoRegister.getName());
        user.setEmail(userPojoRegister.getEmail());
        user.setPassword(userPojoRegister.getPassword());
        userRepository.save(user);

        return userRepository.save(user);
    }

    public boolean existsUserPojo(@PathVariable Register userPojoEmail) {
        return userRepository.findUserByEmail(userPojoEmail) != null;
    }

    public UserDTO changeEmail(String email, String newEmail) {
        try{
            User user = userRepository.findByEmail(email);
            if(user==null){
                throw new InvalidUser();
            }
            user.setEmail(newEmail);
            User updateUser = userRepository.save(user);

            return new UserDTO(updateUser);
        }catch (HttpClientErrorException.BadRequest e){
            throw new InvalidInput();
        }

    }

    public UserDTO changeName(String email, String newUsername) {
        try{
            User user = userRepository.findByEmail(email);
            if(user==null){
                throw new InvalidUser();
            }
            user.setName(newUsername);
            User updateUser = userRepository.save(user);

            return new UserDTO(updateUser);
        }catch (HttpClientErrorException.BadRequest e){
            throw new InvalidInput();
        }
    }

    
    public UserDTO changePassword(String email, String newPassword) {
        try{
            User user = userRepository.findByEmail(email);
            if(user==null){
                throw new InvalidUser();
            }
            user.setPassword(newPassword);
            User updateUser = userRepository.save(user);

            return new UserDTO(updateUser);
        }catch (HttpClientErrorException.BadRequest e){
            throw new InvalidInput();
        }
    }

    public User verifyLogin(Login userPojoLogin){
        User user = userRepository.findByEmail(userPojoLogin.getEmail());
        if(user != null && user.getPassword().equals(userPojoLogin.getPassword())){
            return user;
        }
        throw new InvalidUser();
    }
}
