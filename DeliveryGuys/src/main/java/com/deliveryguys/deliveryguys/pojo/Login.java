package com.deliveryguys.deliveryguys.pojo;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class Login implements Serializable {

    @NotBlank
    private final String password;

    @NotBlank
    @Email
    private final String email;

    public Login(String password, String email) {
        this.password = password;
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

}
