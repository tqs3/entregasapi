package com.deliveryguys.deliveryguys.pojo;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class Register {

    private String name;

    @NotBlank
    private String password;

    @NotBlank
    @Email
    private String email;

    public Register(String name, String password, String email) {
        this.name = name;
        this.password = password;
        this.email = email;
    }

    public Register(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Register{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
