package com.deliveryguys.deliveryguys.controllers;

import com.deliveryguys.deliveryguys.dto.StoreDTO;
import com.deliveryguys.deliveryguys.model.Store;
import com.deliveryguys.deliveryguys.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;

import java.util.List;

@RestController
@RequestMapping("/delivery")
public class StoreController {

    @Autowired
    private StoreService storeService;

    @GetMapping(value="/store/{storeId}")
    @ApiOperation(value ="Get a store's information")
    public Store getStoresById(@PathVariable("storeId") Long storeId) {
        return storeService.getStoresById(storeId);
    }

    @PostMapping("/stores")
    @ApiOperation(value ="Create a store in the database",
    notes = "Insert a name and a type to your store")
    public String createStore(@RequestBody StoreDTO storeDTO) {
        Store newStore = storeService.addStore(storeDTO);
        return newStore.getName();
    }

    @GetMapping("/allStores")
    @ApiOperation(value ="Get all stores available in the database")
    public List<Store> getAllStores() {
        return storeService.getAllStores();
    }

}
