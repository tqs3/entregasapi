package com.deliveryguys.deliveryguys.controllers;

import java.util.List;



import com.deliveryguys.deliveryguys.dto.UserDTO;
import com.deliveryguys.deliveryguys.exceptions.InvalidUser;
import com.deliveryguys.deliveryguys.model.User;
import com.deliveryguys.deliveryguys.pojo.Login;
import com.deliveryguys.deliveryguys.pojo.Register;
import com.deliveryguys.deliveryguys.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/delivery")
public class UserController {
    
    @Autowired
    private UserService userService;


    @GetMapping("/users")
    @ApiOperation(value ="Get all the couriers available in the database")
    public List<User> getUsers(){
        return userService.getUsers();
    }

    @GetMapping("/users/emails")
    @ApiOperation(value ="Get all the courier's emails available")
    public List<String> getEmails() {
        return userService.getEmails();
    }

    @GetMapping("/user/{email}")
    @ApiOperation(value ="Get all the courier's info by his email",
    notes = "Please insert the email at the url to see the courier info ")
    public User getUserByEmail(@PathVariable("email") String email){
        return userService.findByEmail(email);
    }
    
    @GetMapping("/users/{id}")
    @ApiOperation(value ="Get all the courier's info by ID",
    notes = "Please insert the ID at the url to see the courier info ")
    public User getUserByID(@PathVariable(value = "id")long id) throws InvalidUser{
        return userService.getUserByID(id);
    }

    //Get order by id
    @PostMapping("/newUser")
    @ApiOperation(value ="Insert a new user in the database",
    notes = "Insert a name, password and an email to the Courier")
    public String addNewUser(@RequestBody UserDTO userDTO) {
        User newUser = userService.addUser(userDTO);
        return newUser.getName();
    }

    @GetMapping("/user={email}/password")
    public String getPassword(@PathVariable(value = "email") String email) {
        return userService.getPassword(email);
    }
    
    @PutMapping("/edit/user={email}/password")
    @ApiOperation(value ="Change the password for a User")
    public UserDTO changePassword(@PathVariable("email") String email, @RequestBody String newPassword) {
        return userService.changePassword(email, newPassword);
    }

    @PutMapping("/edit/user={email}/name")
    @ApiOperation(value ="Change the Name for a User")
    public UserDTO changeUsername(@PathVariable("email") String email, @RequestBody String newUsername) {
        return userService.changeName(email, newUsername);

    }

    @PutMapping("/edit/user={email}/email")
    public UserDTO changeEmail(@PathVariable("email") String email, @RequestBody String newEmail) {
        return userService.changeEmail(email, newEmail);

    }

    @PostMapping(value = "/user/register")
    @ApiOperation(value ="Register a Courier in the database")
    public ResponseEntity<User> register(@RequestBody Register userRegisterPojo) {
        userService.existsUserPojo(userRegisterPojo);
        User savedUser = userService.saveUserPojo(userRegisterPojo);
        return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
    }

    @PostMapping(path = "/user/login")
    @ApiOperation(value ="Normal authentication for the Courier")
    public ResponseEntity<User> login(@RequestBody Login userLoginPojo) {
        User userVerification = userService.verifyLogin(userLoginPojo);
        return new ResponseEntity<>(userVerification, HttpStatus.OK);
    }
}