package com.deliveryguys.deliveryguys.controllers;

import java.util.List;

import com.deliveryguys.deliveryguys.dto.NewOrderDTO;
import com.deliveryguys.deliveryguys.dto.OrderDTO;
import com.deliveryguys.deliveryguys.model.Order;
import com.deliveryguys.deliveryguys.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;

@CrossOrigin("/delivery")
@RestController
@RequestMapping("/delivery")
public class OrderController {
    

    @Autowired
    private OrderService orderService;


    public OrderController(OrderService orderService){
        this.orderService = orderService;
    }

    @GetMapping(value="/all")
    @ApiOperation(value ="Get All Orders")
    public List<OrderDTO> getAll(){
        return orderService.getAllOrders();
    }

    @PostMapping("/newOrder")
    @ApiOperation(value ="Create a new order information" )
    public Order newOrder(@RequestBody NewOrderDTO newOrderDTO){
        return orderService.newOrder(newOrderDTO);
    }

    @GetMapping(value = "/order={id}")
    @ApiOperation(value ="Get te order details by ID",notes = "")
    public OrderDTO getOrderByID(Long id){
        return orderService.getOrderByID(id);
    }

    @PutMapping(value = "/delivered/order={id}")
    @ApiOperation(value ="Update the State to Delivered in the table",
    notes = "Define the order as true in the delivered section")
    public boolean setDelivered(@PathVariable("id") long id) {
        return orderService.setDelivered(id);
    }

    @GetMapping(value = "/user={email}/delivers")
    @ApiOperation(value ="Get the user's delivered orders")
    public List<OrderDTO> getUserDelivers(@PathVariable(value = "email") String email){
        return orderService.getUserDelivers(email);
    }

    @GetMapping(value = "/order={id}/updateStatus")
    @ApiOperation(value ="Update the Status of the order",
    notes = "The order status can be : 'WAITING','ACCEPTED','IN TRANSIT','DELIVERED'")
    public Order updateStatus(@PathVariable("id") long id) {
        return orderService.verifyOrderStatus(id);
    }
}