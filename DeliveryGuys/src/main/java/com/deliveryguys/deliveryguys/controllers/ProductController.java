package com.deliveryguys.deliveryguys.controllers;

import com.deliveryguys.deliveryguys.dto.ProductDTO;
import com.deliveryguys.deliveryguys.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/delivery")
public class ProductController {

    @Autowired
    private ProductService productService;


    @GetMapping(value="/product/{productId}")
    public List<String> getProductsById(@PathVariable("productId") Long productId) {
        return productService.getProductsById(productId);
    }

    @GetMapping("/products")
    public List<ProductDTO> getAllProduct() {
        return productService.getAllProducts();
    }
}
