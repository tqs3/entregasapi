package com.deliveryguys.deliveryguys.service;

import com.deliveryguys.deliveryguys.pojo.Login;
import com.deliveryguys.deliveryguys.pojo.Register;
import com.deliveryguys.deliveryguys.repository.UserRepository;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.deliveryguys.deliveryguys.dto.UserDTO;
import com.deliveryguys.deliveryguys.exceptions.InvalidUser;
import com.deliveryguys.deliveryguys.model.User;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock(lenient = true)
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    private User user , user1;

    Logger logger = Logger.getLogger(UserServiceTest.class.getName());

    @BeforeEach
        void setUp(){
            String name = "John Doe";
            String pass = "admin";
            String email = "john@delivery.com";

            String name1 = "Marta";
            String pass1 = "admin123";
            String email1 = "marta@delivery.com";

            user = new User(name , pass , email);
            user1 = new User(name1, pass1,email1);

            //TODO: SETID TO USER

            List<User> users = new ArrayList<>();
            //List<User> allusers = Arrays.asList(user,user1);
            users.add(user);
            users.add(user1);
            //List<String> usernames =Arrays.asList(user.getName(), user1.getName());
            List<String> useremails =Arrays.asList(user.getEmail(), user1.getEmail());
            Mockito.when(userRepository.findByEmail(user.getEmail())).thenReturn(user);
            Mockito.when(userRepository.getEmail(user.getName())).thenReturn(email);
            Mockito.when(userRepository.getPassword(user.getEmail())).thenReturn(pass);
            Mockito.when(userRepository.getUsers()).thenReturn(users);
            Mockito.when(userRepository.getEmails()).thenReturn(useremails);
            Mockito.when(userRepository.findByEmail(user.getEmail())).thenReturn(user);
            

        }

        @Test
        void whenPostUser_thenUserShouldBeSaved() {


            String email="jose@email.com";
            String password="password";
            String nome="Jose";
    
            User user= new User(nome,password,email);
            Mockito.when(userRepository.save(user)).thenReturn(user);
            User saved = userService.save(user);
            assertThat(saved.getEmail()).isEqualTo(user.getEmail());
            assertThat(saved.getPassword()).isEqualTo(user.getPassword());
            
    
        }

       @Test
        void whenValidEmail_thenUserShouldBeFound(){
            String email1 = "john@delivery.com";
            User found= userRepository.findByEmail(email1);
            assertThat(found.getEmail()).isEqualTo(email1);
    
        }

        private void verifyFindByEmailIsCalledOnce(String email) {
            Mockito.verify(userRepository, VerificationModeFactory.times(1)).findByEmail(email);
            Mockito.reset(userRepository);
        }
        
        @Test
        void whenInValidEmail_thenUserShouldNotBeFound() {
        User fromDb = userService.findByEmail("wrong_email");
        assertThat(fromDb).isNull();

        verifyFindByEmailIsCalledOnce("wrong_email");
    }

    @Test
    void getUsers(){
        List<User> users = userService.getUsers();
        assertThat(users)
                .hasSize(2)
                .extracting(User::getName)
                .contains("John Doe","Marta");
    }

    @Test
    void getEmails(){
        List<String> emails = userService.getEmails();
        assertThat(emails)
                .hasSize(2)
                .extracting(String::toString)
                .contains(user.getEmail(), user1.getEmail());
    }

    @Test
    void getPassword_withValidEmail(){
        String pass = userService.getPassword(user.getEmail());
        assertThat(pass)
                .isEqualTo(user.getPassword());
    }

    @Test
    void getEmail_withValidUsername(){
        String email = userService.getEmail(user.getName());
        assertThat(email)
                .isEqualTo(user.getEmail());
    }

    @Test
    void addNewUser_withValidDTO(){
        UserDTO userDTO = new UserDTO("nuno", "test", "nuno@ua.pt");
        User newUser = userService.addUser(userDTO);
        assertThat(newUser.getName()).isEqualTo(userDTO.getName());
        assertThat(newUser.getEmail()).isEqualTo(userDTO.getEmail());
        assertThat(newUser.getPassword()).isEqualTo(userDTO.getPassword());
    }

    @Test
    void getEmail_withInvalidUsername(){
        assertThrows(InvalidUser.class, ()->userService.getEmail("blabla"));
    }

    @Test
    void whenValidEmail_ChangeName(){
        User user2 = new User("pedro", "c", "pedro@ua.pt");
        user2.setName("Pedro123");
        assertEquals("Pedro123", user2.getName());
        assertEquals("pedro@ua.pt", user2.getEmail());
    }


    @Test
    void getPassword_withInvalidUsername(){
        assertThrows(InvalidUser.class, ()->userService.getPassword("blabla"));
    }

    @Test
    void whenMailAndPasswordCorrespond_ThenReturnCourier_existsPoko(){
        Register userRegister = new Register("pedro@ua.pt","c", "pedro@ua.pt");
        User user2 = new User("pedro", "c", "pedro@ua.pt");

        Mockito.when(userRepository.findByEmail(Mockito.any())).thenReturn(user2);
        boolean res_courier = userService.existsUserPojo(userRegister);

        assertThat(userRegister.getEmail()).isEqualTo(user2.getEmail());
        assertThat(userRegister.getPassword()).isEqualTo(user2.getPassword());
    }
}