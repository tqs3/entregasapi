package com.deliveryguys.deliveryguys.service;

import com.deliveryguys.deliveryguys.dto.StoreDTO;
import com.deliveryguys.deliveryguys.exceptions.InvalidStoreId;
import com.deliveryguys.deliveryguys.model.Store;
import com.deliveryguys.deliveryguys.repository.StoreRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.HttpClientErrorException;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class StoreServiceTest {

    @Mock(lenient = true)
    private StoreRepository storeRepository;

    @InjectMocks
    private StoreService storeService;

    private Store store1;
    private Store store2;
    private StoreDTO store3;
    private StoreDTO store4;
    private Store store5;
    private StoreDTO store6;

    private long invalidStoreId = 999999;

    @BeforeEach
    void setUp() {

        store1 = new Store("Nova Farmácia", "Saúde e Beleza");
        store2 = new Store("Farmácia Avenida", "Saúde e Beleza");
        store3 = new StoreDTO(store1);
        store4 = new StoreDTO(store2);

        //List<StoreDTO> allStores = Arrays.asList(store3, store4);
        List<String> allStoresNames = Arrays.asList(store1.getName(), store2.getName());

        List<Store> allStores = new ArrayList<>();
        allStores.add(store1);
        allStores.add(store2);

        Mockito.when(storeRepository.getAllStores()).thenReturn(allStores);
        //Mockito.when(storeRepository.getStoresById(store1.getId())).thenReturn(allStoresNames);
        Mockito.when(storeRepository.getStoresById(invalidStoreId)).thenThrow(HttpClientErrorException.NotFound.class);
    }

    @Test
    void getStores() {
        List<Store> allStores = storeService.getAllStores();
        assertThat(allStores).hasSize(2);
        assertThat(allStores.get(0).getName()).isEqualTo(store1.getName());
        assertThat(allStores.get(1).getName()).isEqualTo(store2.getName());
    }

    //java.lang.AssertionError:
    //Expected size: 1 but was: 2 in:
    //["Nova Farmácia", "Farmácia Avenida"]
    /*@Test
    void getStoresById() {
        /*List<String> Store allStores = storeService.getStoresById(store1.getId());
        assertThat(allStores)
                .hasSize(2)
                .extracting(String::toString)
                .contains(store1.getName(), store2.getName());
    }*/

    @Test
    void addStore() {
        store5 = new Store("Farmácia Estácio", "Saúde e Beleza");
        store6 = new StoreDTO(store5);
        Store store = new Store(store6);
        Mockito.when(storeRepository.save(store)).thenReturn(store);
        Store saved = storeService.save(store);
        assertThat(saved.getName()).isEqualTo(store.getName());
    }

    @Test
    void getStoresById_withInvalidId() {
        assertThrows(InvalidStoreId.class, ()-> storeService.getStoresById(invalidStoreId));
    }

}
