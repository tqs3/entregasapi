package com.deliveryguys.deliveryguys.service;

import com.deliveryguys.deliveryguys.dto.ProductDTO;
import com.deliveryguys.deliveryguys.model.Product;
import com.deliveryguys.deliveryguys.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @Mock(lenient = true)
    private ProductRepository productRepository;

    @InjectMocks
    private ProductService productService;

    private Product product1;
    private Product product2;
    private ProductDTO product3;
    private ProductDTO product4;

    @BeforeEach
    void setUp() {

        product1 = new Product();
        product2 = new Product();
        product3 = new ProductDTO(product1);
        //product4 = new ProductDTO(product2);

        List<Product> allProducts = new ArrayList<>();
        allProducts.add(product1);
        allProducts.add(product2);

        Mockito.when(productRepository.getAllProducts()).thenReturn(allProducts);
        Mockito.when(productRepository.getProductsById(null)).thenReturn(null);
    }

    @Test
    void getProducts() {
        List<ProductDTO> allProducts = productService.getAllProducts();
        assertThat(allProducts).hasSize(2);
    }

    //TODO: Revisitar este teste
    @Test
    void getProductsById() {
        //getProductsById(product2.getId());
        List<String> allProductsIds = productService.getProductsById(product3.getProductId());
        assertThat(allProductsIds)
                .isEmpty();
    }
}
