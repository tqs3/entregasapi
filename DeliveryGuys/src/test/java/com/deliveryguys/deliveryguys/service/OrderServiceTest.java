package com.deliveryguys.deliveryguys.service;

import com.deliveryguys.deliveryguys.dto.NewOrderDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.deliveryguys.deliveryguys.dto.OrderDTO;
import com.deliveryguys.deliveryguys.model.Order;
import com.deliveryguys.deliveryguys.model.User;
import com.deliveryguys.deliveryguys.repository.OrderRepository;
import com.deliveryguys.deliveryguys.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {

    @Mock(lenient = true)
    private OrderRepository orderRepository;

    @Mock(lenient = true)
    private UserRepository userRepository;

    @InjectMocks
    private OrderService orderService;

    private Order order1, order2;
    private NewOrderDTO ordDTO1, ordDTO2;

    private User user1;

    @BeforeEach
    void setUp(){
        Date data = new Date(2020-10-05);
        ordDTO1 = new NewOrderDTO(1L, 2L, data.toString(), true, "WAITING");
        ordDTO2 = new NewOrderDTO(2L, 2L, data.toString(), true, "WAITING");

        user1 = new User("jose", "admin", "jose@delivery.com");
        user1.setID(1L);

        order1 = new Order(ordDTO1, user1);
        order2 = new Order(ordDTO2, user1);
        order1.setId(111L);

        List<Order> orders = Arrays.asList(order1,order2);
        List<Order> delivered = Arrays.asList(order1, order2);
        //boolean estado = order1.State();
        //data = order1.getDate();
        
        List<NewOrderDTO> allOrderDTOs = new ArrayList<>();
        allOrderDTOs.add(ordDTO1);
        allOrderDTOs.add(ordDTO2);
        

        Mockito.when(orderRepository.getAllOrders()).thenReturn(orders);
        Mockito.when(orderRepository.findByID(order1.getId())).thenReturn(order1);
        
        Mockito.when(orderRepository.save(order1)).thenReturn(order1);
        Mockito.when(orderRepository.save(order2)).thenReturn(order2);
        Mockito.when(userRepository.findByEmail(user1.getEmail())).thenReturn(user1);

        Mockito.when(orderRepository.getUserDelivers(user1.getUserID())).thenReturn(delivered);
    }

    @Test
     void getAllOrders(){
         List<OrderDTO> allOrders = orderService.getAllOrders();
         assertThat(allOrders).hasSize(2);
         assertThat(allOrders.get(0).getDate()).isEqualTo(ordDTO1.getDate());
         assertThat(allOrders.get(1).getDate()).isEqualTo(ordDTO2.getDate());
     }

     @Test
     void whenValidOrderID_GetOrder(){
        OrderDTO orderDTO = orderService.getOrderByID(order1.getId());
        assertThat(orderDTO.isState()).isEqualTo(ordDTO1.isState());
     }

     @Test
    void setDelivered_withValidID(){
        boolean delivered = orderService.setDelivered(order1.getId());
        assertThat(delivered).isTrue();
    }
}
