package com.deliveryguys.deliveryguys.repository;


import java.util.List;

import com.deliveryguys.deliveryguys.model.User;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
@AutoConfigureTestDatabase
class UserRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private UserRepository userRepository;

    User user1;
    User user2;

    
    @BeforeEach
    void setUp(){
            String name = "John Doe";
            String pass = "admin";
            String email = "john@delivery.com";

            String name1 = "Marta";
            String pass1 = "admin123";
            String email1 = "marta@delivery.com";

            user1 = new User(name , pass , email);
            user2 = new User(name1, pass1,email1);

            testEntityManager.persistAndFlush(user1);
            testEntityManager.persistAndFlush(user2);
    }

    @Test
     void whenInvalidId_thenReturnNull() {
        User fromDb = userRepository.findByID((long)1000);
        assertThat(fromDb).isNull();
    }
    @Test
    void getEmails(){
        List<String> allEmails = userRepository.getEmails();
        assertThat(allEmails.size()).isEqualTo(2);
        assertThat(allEmails.get(0)).isEqualTo(user1.getEmail());
        assertThat(allEmails.get(1)).isEqualTo(user2.getEmail());
    }

    @Test
    void getPassword(){
        String password = userRepository.getPassword(user1.getEmail());
        assertThat(password).isEqualTo(user1.getPassword());
    }

    @Test
    void findIdByEmail(){
        long userID = userRepository.findIdByEmail(user1.getEmail());
        assertThat(userID).isEqualTo(user1.getUserID());
    }



    @Test
    void getUsers(){
        List<User> allUsernames = userRepository.getUsers();
        assertThat(allUsernames.size()).isEqualTo(2);
        assertThat(allUsernames.get(0).getName()).isEqualTo(user1.getName());
        assertThat(allUsernames.get(1).getName()).isEqualTo(user2.getName());
    }

}
