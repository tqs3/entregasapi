package com.deliveryguys.deliveryguys.repository;

import com.deliveryguys.deliveryguys.dto.StoreDTO;
import com.deliveryguys.deliveryguys.model.Store;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class StoreRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private StoreRepository storeRepository;

    private Store store1;
    private Store store2;
    private StoreDTO store3;
    private StoreDTO store4;
    private Store store5;
    private StoreDTO store6;

    private long invalidStoreId = 999999;

    @BeforeEach
    void setUp() {

        store1 = new Store("Nova Farmácia", "Saúde e Beleza");
        store2 = new Store("Farmácia Avenida", "Saúde e Beleza");
        store3 = new StoreDTO(store1);
        store4 = new StoreDTO(store2);

        //List<StoreDTO> allStores = Arrays.asList(store3, store4);
        List<String> allStoresNames = Arrays.asList(store1.getName(), store2.getName());

        List<Store> allStores = new ArrayList<>();
        allStores.add(store1);
        allStores.add(store2);

        testEntityManager.persistAndFlush(store1);
        testEntityManager.persistAndFlush(store2);
    }

    @Test
    void getStores() {
        List<Store> allStores = storeRepository.getAllStores();
        assertThat(allStores.size()).isEqualTo(2);
        assertThat(allStores.get(0).getName()).isEqualTo(store1.getName());
        assertThat(allStores.get(1).getName()).isEqualTo(store2.getName());
    }

    @Test
    void getStoresById() {
        Store allStores = storeRepository.getStoresById(store1.getId());
        assertThat(allStores.getId()).isEqualTo(store1.getId());
    }
}
