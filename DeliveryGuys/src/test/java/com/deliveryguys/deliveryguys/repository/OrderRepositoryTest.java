package com.deliveryguys.deliveryguys.repository;

import static org.assertj.core.api.Assertions.assertThat;


import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import com.deliveryguys.deliveryguys.dto.OrderDTO;
import com.deliveryguys.deliveryguys.model.Order;
import com.deliveryguys.deliveryguys.model.Store;
import com.deliveryguys.deliveryguys.model.User;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

@DataJpaTest
@AutoConfigureTestDatabase
class OrderRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private OrderRepository orderRepository;

    private OrderDTO ordDTO1, ordDTO2;

    private Order order1, order2;
    private Store store;
    private User user1;


    @BeforeEach
    void setUp() {
        Date data = new Date(2020-10-05);;
        ordDTO1 = new OrderDTO(data, false, "WAITING");
        ordDTO2 = new OrderDTO(data,true, "WAITING");

        user1 = new User("jose", "admin", "jose@delivery.com");
        store = new Store("nome","Saude e Beleza");
        order1 = new Order(ordDTO1, user1);
        order2 = new Order(ordDTO2, user1);

        //order1.setId(111L);
        //order2.setId(112L);
        store.setId(111L);
        store.addOrder(order1);
        store.addOrder(order2);


        //List<Order> orders = Arrays.asList(order1,order2);
        //List<Order> delivered = Arrays.asList(order2);
        //boolean estado = order1.State();
        

        testEntityManager.persistAndFlush(user1);
        testEntityManager.persistAndFlush(order1);
        testEntityManager.persistAndFlush(order2);

}

        @Test
        void getAllOrders(){
        List<Order> allOrders = orderRepository.getAllOrders();
        //assertThat(allOrders).hasSize(2);
        assertThat(allOrders.get(0).getDate()).isEqualTo(order1.getDate());
        assertThat(allOrders.get(1).getDate()).isEqualTo(order2.getDate());
    }
    
    @Test
    void findByID(){
        Order or = orderRepository.findByID(order1.getId());
        assertThat(or.getUserID()).isEqualTo(order1.getUserID());
        
    }
}

