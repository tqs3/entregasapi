package com.deliveryguys.deliveryguys.controller;

import com.deliveryguys.deliveryguys.ToJSON;
import com.deliveryguys.deliveryguys.controllers.StoreController;
import com.deliveryguys.deliveryguys.dto.StoreDTO;
import com.deliveryguys.deliveryguys.dto.UserDTO;
import com.deliveryguys.deliveryguys.exceptions.InvalidStoreId;
import com.deliveryguys.deliveryguys.model.Store;
import com.deliveryguys.deliveryguys.model.User;
import com.deliveryguys.deliveryguys.repository.StoreRepository;
import com.deliveryguys.deliveryguys.service.StoreService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.springframework.http.MediaType;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(StoreController.class)
class StoreControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    StoreService storeService;

    private StoreDTO store3;
    private StoreDTO store4;

    @Test
    void createStore() throws Exception {
        StoreDTO store= new StoreDTO("Nova Farmácia", "Saúde e Beleza");

        given(storeService.addStore(any(StoreDTO.class))).willReturn(new Store(store));

        mockMvc.perform(post("/delivery/stores")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJSON.ToJson(store)))
                .andExpect(content().string("Nova Farmácia"))
                .andExpect(status().isOk());
    }

    @Test
    void getAllStores() throws Exception {
        List<Store> stores = new ArrayList<>();

        Store store = new Store("Farmácia Avenida", "Saúde e Beleza");
        Store store_1 = new Store("Farmácia Avenida", "Saúde e Beleza");

        store3 = new StoreDTO(store);
        store4 = new StoreDTO(store_1);
        stores.add(store);
        stores.add(store_1);

        given(storeService.getAllStores()).willReturn(stores);


        mockMvc.perform(get("/delivery/allStores").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is(store.getName())));
        verify(storeService, VerificationModeFactory.times(1)).getAllStores();
        reset(storeService);
    }

    @Test
    void getStoresById_AndStoreIdIsValid_thenReturn() throws Exception {
        long storeId = 1;

        Store stores = new Store("Nova Farmácia", "Saude e Beleza");

        given(storeService.getStoresById(any())).willReturn(stores);
        mockMvc.perform(get("/delivery/store/" + storeId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Nova Farmácia")));
    }

    @Test
    void getStoresById_AndIdIsInvalid() throws Exception {
        long storeId = 9999999;
        given(storeService.getStoresById(any())).willThrow(InvalidStoreId.class);
        mockMvc.perform(get("/delivery/store/" + storeId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }
}
