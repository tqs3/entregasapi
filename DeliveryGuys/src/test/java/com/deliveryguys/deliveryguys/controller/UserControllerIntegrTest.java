package com.deliveryguys.deliveryguys.controller;


import com.deliveryguys.deliveryguys.DeliveryGuysApplication;
import com.deliveryguys.deliveryguys.ToJSON;
import com.deliveryguys.deliveryguys.model.User;
import com.deliveryguys.deliveryguys.pojo.Register;
import com.deliveryguys.deliveryguys.repository.UserRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import org.springframework.test.web.servlet.MockMvc;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = DeliveryGuysApplication.class)
@AutoConfigureMockMvc
class UserControllerIntegrTest {
    

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    private User user1;
    private User user2;

    @BeforeEach
    void setUp(){
        userRepository.deleteAll();
        user1 = new User ("nuno", "pass", "nuno@delivery");
        user2 = new User ("jose", "admin", "jose@delivery");
        userRepository.save(user1);
        userRepository.save(user2);
    }


    @Test
    void whenChangeUsersPassword_thenReturnUserDTO() throws Exception {
        
        String newPassword = "pass2";
        
        mockMvc.perform(put("/delivery/edit/user=" + user1.getEmail() + "/password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newPassword))
                .andExpect(jsonPath("$.email", is(user1.getEmail())))
                .andExpect(jsonPath("$.name", is(user1.getName())))
                .andExpect(jsonPath("$.password", is(newPassword)))
                .andExpect(status().isOk());
    }

    @Test
    void whenChangeUsersUsername_thenReturnUserDTO() throws Exception {
        String newUsername = "NUNOO";

        mockMvc.perform(put("/delivery/edit/user=" + user1.getEmail() + "/name")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newUsername))
                .andExpect(jsonPath("$.name", is(newUsername)))
                .andExpect(jsonPath("$.email", is(user1.getEmail())))
                .andExpect(jsonPath("$.password", is(user1.getPassword())))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetAllUsers_thenReturnArrayJSON() throws Exception {
        mockMvc.perform(get("/delivery/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is("nuno")))
                .andExpect(jsonPath("$[1].name", is("jose")));
    }

   
    
}
