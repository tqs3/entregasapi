package com.deliveryguys.deliveryguys.controller;

import com.deliveryguys.deliveryguys.dto.NewOrderDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.deliveryguys.deliveryguys.ToJSON;
import com.deliveryguys.deliveryguys.controllers.OrderController;
import com.deliveryguys.deliveryguys.dto.OrderDTO;
import com.deliveryguys.deliveryguys.exceptions.InvalidInput;
import com.deliveryguys.deliveryguys.model.Order;
import com.deliveryguys.deliveryguys.model.User;
import com.deliveryguys.deliveryguys.service.OrderService;


@WebMvcTest(OrderController.class)
class OrderControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    OrderService orderService;

    @Test
    void whenGetOrderByID_ReturnOrderDTO() throws Exception {
        Date data = new Date();

        User user1 = new User("jose", "admin", "jose@delivery.com");
        Order order1 = new Order(data.toString(), false);
        order1.setId(111L);

        OrderDTO ordDT= new OrderDTO(5L, "j@ua.pt", order1.getDate(), false,1L, "WAITING");
        System.out.println(ordDT);

        given(orderService.getOrderByID(any())).willReturn(ordDT);
        mockMvc.perform(get("/delivery/order=111L")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.date", is(ordDT.getDate())))
        //.andExpect(jsonPath("$.state", is(ordDT.State())))
        .andExpect(status().isOk());
    }

    @Test
    void whenGetAll_thenReturnListOrderDTO() throws Exception {
        Date data = new Date(2020-10-05);

        User user1 = new User("jose", "admin", "jose@delivery.com");
        
        Order order1 = new Order(data.toString(), false);
        Order order3 = new Order(data.toString(),false);
        order1.setId(11L);
      
        order3.setId(12L);
        OrderDTO ordDTO1= new OrderDTO(order1, user1);
        OrderDTO ordDTO2= new OrderDTO(order3, user1);
        List<OrderDTO> orders = Arrays.asList(ordDTO1,ordDTO2);
        

        

        given(orderService.getAllOrders()).willReturn(orders);
        mockMvc.perform(get("/delivery/all")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    void whenAddNewOrder_thenReturnTrue() throws Exception {
        Date time = new Date(2020-10-05);
        Order order1 = new Order(time.toString(), false);
        User user1 = new User("jose", "admin", "jose@delivery.com");
        user1.setID(11L);
        NewOrderDTO ordDT= new NewOrderDTO(1L, 2L, time.toString(), false, "WAITING");

        given(orderService.newOrder(any(NewOrderDTO.class))).willReturn(order1);
        mockMvc.perform(post("/delivery/newOrder")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJSON.ToJson(ordDT)))
                .andExpect(jsonPath("$.date", is(order1.getDate())))
                .andExpect(jsonPath("$.userID", is(order1.getUserID())))
                .andExpect(status().isOk());
    }

    @Test
    void whenAddNewOrder_AndContentInvalid_thenReturnBadRequest() throws Exception {
        //LocalDateTime time = LocalDateTime.now();
        Date time = new Date(2020-10-05);
        Order order = new Order(time.toString(),false);
        NewOrderDTO ordDT= new NewOrderDTO(1L, 2L, time.toString(), false, "INVALID");

        given(orderService.newOrder(any(NewOrderDTO.class))).willThrow(InvalidInput.class);
        mockMvc.perform(post("/delivery/newOrder")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJSON.ToJson(order)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenSetDelivered_thenReturnFalse() throws Exception {
        long orderID = 1123341248;

        given(orderService.setDelivered(orderID)).willReturn(false);
        mockMvc.perform(put("/delivery/delivered/order="+orderID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("false"))
                .andExpect(status().isOk());
    }

    @Test
    void whenSetDelivered_thenReturnTrue() throws Exception {
        long orderID = 1;

        given(orderService.setDelivered(orderID)).willReturn(true);
        mockMvc.perform(put("/delivery/delivered/order="+orderID)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("true"))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetUserSells_thenReturnListOrderSearchDTO() throws Exception {
        Date data = new Date(2020-10-05);
        //OrderDTO ordDTO1 = new OrderDTO((long)1,"nuno@delivery.pt",data.toString(), true);
        //OrderDTO ordDTO2 = new OrderDTO((long)2,"nuno@delivery.pt", data.toString(),true);

        User user1 = new User("jose", "admin", "jose@delivery.com");
        
        Order order1 = new Order(data.toString(), false);
        Order order3 = new Order(data.toString(),false);
        order1.setId(11L);
      
        order3.setId(12L);
        OrderDTO ordDTO1= new OrderDTO(order1, user1);
        OrderDTO ordDTO2= new OrderDTO(order3, user1);
        List<OrderDTO> orders = Arrays.asList(ordDTO1,ordDTO2);

        

        given(orderService.getUserDelivers(any())).willReturn(orders);
        mockMvc.perform(get("/delivery/user="+ ordDTO1.getEmail()+"/delivers")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].orderID", is(11)))
                .andExpect(jsonPath("$[0].email", is(ordDTO1.getEmail())))
                .andExpect(jsonPath("$[0].date", is(ordDTO1.getDate())))
                //.andExpect(jsonPath("$[0].state", is(adDTO1.getBrand())))
                .andExpect(jsonPath("$[1].orderID", is(12)))
                .andExpect(jsonPath("$[1].email", is(ordDTO2.getEmail())))
                .andExpect(jsonPath("$[1].date", is(ordDTO2.getDate())));
                
    }

    @Test
    void updateStatus() throws Exception {
        Date data = new Date();
        User user1 = new User("jose", "admin", "jose@delivery.com");
        Order order1 = new Order(data.toString(), false);

        order1.setId(11L);
        order1.setStatus("IN TRANSIT");

        OrderDTO ordDTO1= new OrderDTO(11L, "j@ua.pt", data, false,1L, "ACCEPTED");


        OrderDTO ordDTO2 = new OrderDTO(order1, user1);


        given(orderService.verifyOrderStatus(ordDTO1.getOrderID())).willReturn(order1);
        mockMvc.perform(get("/delivery/order="+ordDTO1.getOrderID() +"/updateStatus")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJSON.ToJson(ordDTO1)))
                .andExpect(jsonPath("$.status", is(order1.getStatus())))
                .andExpect(status().isOk());
    }
}
