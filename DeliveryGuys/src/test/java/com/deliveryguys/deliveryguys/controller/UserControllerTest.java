package com.deliveryguys.deliveryguys.controller;


import com.deliveryguys.deliveryguys.exceptions.InvalidLogin;
import com.deliveryguys.deliveryguys.exceptions.InvalidRegister;
import com.deliveryguys.deliveryguys.exceptions.InvalidUser;
import com.deliveryguys.deliveryguys.pojo.Login;
import com.deliveryguys.deliveryguys.pojo.Register;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.deliveryguys.deliveryguys.ToJSON;
import com.deliveryguys.deliveryguys.controllers.UserController;
import com.deliveryguys.deliveryguys.dto.UserDTO;
import com.deliveryguys.deliveryguys.exceptions.InvalidInput;
import com.deliveryguys.deliveryguys.model.User;
import com.deliveryguys.deliveryguys.service.UserService;


@WebMvcTest(UserController.class)
class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserService userService;


    @Test
    void whenGetAllEmails_thenReturnArrayJSON() throws Exception{
        List<String> emails = new ArrayList<>(Arrays.asList("jose@delivery.pt", "marta@delivery.pt"));

        given(userService.getEmails()).willReturn(emails);
        mockMvc.perform(get("/delivery/users/emails")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0]", is("jose@delivery.pt")))
                .andExpect(jsonPath("$[1]", is("marta@delivery.pt")));
    }

    

    @Test
    void WhenfindByEmail_IfUserExists_thenReturnUser() throws Exception{
       String email="jose@email.com";
       String password="password";
       String Name="Jose";
        
       User user= new User(Name, password, email);
       user.setID(1L);
       given(userService.findByEmail(email)).willReturn(user);
       mockMvc.perform(get("/delivery/user/"+ email).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
               .andExpect(jsonPath("$.email", is(user.getEmail())));
       verify(userService, VerificationModeFactory.times(1)).findByEmail(email);
       reset(userService);
    }

    @Test
    void whenNewValidUserSignIn_thenUserAdded() throws Exception {
        UserDTO userDTO = new UserDTO("nuno", "pass", "nuno@delivery");

        given(userService.addUser(any(UserDTO.class))).willReturn(new User(userDTO));
        mockMvc.perform(post("/delivery/newUser")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJSON.ToJson(userDTO)))
                .andExpect(content().string("nuno"))
                .andExpect(status().isOk());
    }

    @Test
    void whenNewUserSignIn_AndContentInvalid_thenReturnBadRequest() throws Exception {

        given(userService.addUser(any(UserDTO.class))).willThrow(InvalidInput.class);
        mockMvc.perform(post("/delivery/newUser")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJSON.ToJson("")))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenGetUsersPassword_thenReturnPassword() throws Exception{
        User user = new User("nuno", "pass", "nuno@ua.pt");
        String pass = "pass";

        given(userService.getPassword(user.getEmail())).willReturn(user.getPassword());
        mockMvc.perform(get("/delivery/user=" + user.getEmail() + "/password")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(pass))
                .andExpect(status().isOk());
    }

    @Test
    void whenChangeUserUsername_AndContentInvalid_thenReturnBadRequest() throws Exception {
        String user = "teste"; //should've been email

        given(userService.changeName(any(), any())).willThrow(InvalidInput.class);
        mockMvc.perform(put("/delivery/edit/user=" + user + "/name")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ToJSON.ToJson("")))
                .andExpect(status().isBadRequest());
    }
    @Test
    void whenChangeUserName_thenReturnUserDTO() throws Exception {
        UserDTO userDTO = new UserDTO("nuno","pass","nuno@delivery.pt");
        String email = "nunoM@delivery.pt";

        given(userService.changeName(any(), any())).willReturn(userDTO);
        mockMvc.perform(put("/delivery/edit/user=" + email + "/name")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userDTO.getName()))
                .andExpect(jsonPath("$.email", is(userDTO.getEmail())))
                .andExpect(jsonPath("$.name", is(userDTO.getName())))
                .andExpect(jsonPath("$.password", is(userDTO.getPassword())))
                .andExpect(status().isOk());
    }

    @Test
    void registerSuccessful() throws Exception {
        Register userRegisterPojo;
        userRegisterPojo = new Register("Jose","123456789", "jose@delivery.pt");
        User user;
        user = new User("Jose","123456789", "jose@delivery.pt");

        when(userService.existsUserPojo(Mockito.any())).thenReturn(true);
        when(userService.saveUserPojo(Mockito.any())).thenReturn(user);
        mockMvc.perform((post("/delivery/user/register")
                .contentType(MediaType.APPLICATION_JSON).content(ToJSON.ToJson(userRegisterPojo))))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(user.getName())));
        verify(userService, times(1)).saveUserPojo(Mockito.any());
    }
    
    @Test
    void registerExistingEmail() throws Exception {
        Register userRegisterPojo;
        userRegisterPojo = new Register("Jose","123456789", "jose@delivery.pt");

        doThrow(InvalidRegister.class).when(userService).existsUserPojo(Mockito.any());
        mockMvc.perform((post("/delivery/user/register")
                .contentType(MediaType.APPLICATION_JSON).content(ToJSON.ToJson(userRegisterPojo))))
                .andExpect(status().isImUsed())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof InvalidRegister));
        verify(userService, times(1)).existsUserPojo(Mockito.any());
    }


    @Test
    void loginSuccessful() throws Exception {
        User user;
        user = new User("Jose","123456789", "jose@delivery.pt");
        Login userLoginPojo;
        userLoginPojo = new Login("123456789", "jose@delivery.pt");

        when(userService.verifyLogin(Mockito.any())).thenReturn(user);
        mockMvc.perform((post("/delivery/user/login")
                .contentType(MediaType.APPLICATION_JSON).content(ToJSON.ToJson(userLoginPojo))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Jose")));
        verify(userService, times(1)).verifyLogin(Mockito.any());
    }


    @Test
    void loginAndPasswordNotMatch() throws Exception{
        Login userLoginPojo;
        userLoginPojo = new Login("123456789", "jose@delivery.pt");

        doThrow(InvalidLogin.class).when(userService).verifyLogin(Mockito.any());
        mockMvc.perform((post("/delivery/user/login")
                .contentType(MediaType.APPLICATION_JSON).content(ToJSON.ToJson(userLoginPojo))))
                .andExpect(status().isUnauthorized())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof InvalidLogin));

        verify(userService, times(1)).verifyLogin(Mockito.any());
    }

    @Test
    void whenValidEmail_ChangeEmail() throws Exception {
        UserDTO userDTO = new UserDTO("nuno","pass","nuno@ua.pt");
        String email = "nunoM@ua.pt";

        given(userService.changeEmail(any(), any())).willReturn(userDTO);
        mockMvc.perform(put("/delivery/edit/user=" + email + "/email")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userDTO.getEmail()))
                .andExpect(jsonPath("$.email", is(userDTO.getEmail())))
                .andExpect(jsonPath("$.name", is(userDTO.getName())))
                .andExpect(jsonPath("$.password", is(userDTO.getPassword())))
                .andExpect(status().isOk());
    }

    @Test
    void LoginValidCredentialsSuccessful() throws Exception{
        User user = new User("Jose","123456789", "jose@delivery.pt");
        Login userLogin = new Login("123456789", "jose@delivery.pt");

        when(userService.verifyLogin(Mockito.any())).thenReturn(user);
        mockMvc.perform((post("/delivery/user/login")
                .contentType(MediaType.APPLICATION_JSON).content(ToJSON.ToJson(userLogin))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Jose")));
        verify(userService, times(1)).verifyLogin(Mockito.any());
    }
}

